﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using UnitTestTrainingConsoleApp.Controller;
using UnitTestTrainingConsoleApp.Entities;
using UnitTestTrainingConsoleApp.Mockup;
using Xunit;

namespace UnitTestTrainingConsoleApp.UnitTest.Controller
{

    
    public class UsersUnitTestController
    {
        

        [Fact]
        public void User_ShouldBe_Return_Object() {

            // Arrange
            var output = new OutputModel();

            // Act
            output = UserMockup.Ins.GetAllUsers();
            var jsonU = JsonSerializer.Serialize(output.Data);
            var count = JsonSerializer.Deserialize<List<Users>>(jsonU);
            

            // Assert
            // output is not null
            Assert.NotNull(output);

            // output is type object
            Assert.IsType<OutputModel>(output);

            // output is equal 3.
            Assert.Equal(3, count.Count);

        }

        [Fact]
        public void Users_ShouldBe_Return_One() {
            var output = new OutputModel();

            output = UserMockup.Ins.GetUserByUsername("test");
            var jsonU = JsonSerializer.Serialize(output.Data);
            var count = JsonSerializer.Deserialize<Users>(jsonU);

            Assert.NotNull(output);
            Assert.Equal(1,output.Code);
        }

        [Fact]
        public void User_Get_One_Should_Be_Error()
        {
            var output = new OutputModel();

            output = UserMockup.Ins.GetUserByUsername("");

            Assert.Null(output.Data);
            Assert.Equal("Error", output.Message);
            Assert.Equal(-1,output.Code);
        }

        [Fact]
        public void User_Should_Be_IsActive()
        {
            var output = new OutputModel();

            output = UserMockup.Ins.GetAllUsers();
            var jsonU = JsonSerializer.Serialize(output.Data);
            var count = JsonSerializer.Deserialize<List<Users>>(jsonU);

            foreach (var user in count) {
                if (user.IsActive) {
                    Assert.Equal(true, user.IsActive);
                }
            }
        }
    }
}

