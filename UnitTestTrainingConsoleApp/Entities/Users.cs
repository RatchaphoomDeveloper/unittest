﻿using System;
namespace UnitTestTrainingConsoleApp.Entities
{
    public class Users
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
        public bool IsActive { get; set; }

        public Users() { }
    }
}

