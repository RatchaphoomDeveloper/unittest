﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestTrainingConsoleApp.Controller;
using UnitTestTrainingConsoleApp.Entities;

namespace UnitTestTrainingConsoleApp.Mockup
{
    public class UserMockup
    {
        OutputModel output;

        private static readonly List<Users> userM = new List<Users>() {
                    new Users(){
                        Name = "Ratchaphoom",
                        Surname = "Boonaka",
                        Username = "test",
                        Password = "1234",
                        Age = 27,
                        IsActive = true
                    },
                    new Users(){
                        Name = "Jay",
                        Surname = "Chou",
                        Username = "jtest",
                        Password = "1234",
                        Age = 60,
                        IsActive = true
                    },
                    new Users(){
                        Name = "Bird",
                        Surname = "Thongchai",
                        Username = "btest",
                        Password = "1234",
                        Age = 60,
                        IsActive = false
                    }
                };

        private static UserMockup _instance = null;
        public static UserMockup Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserMockup();
                }

                return _instance;
            }
        }

        public OutputModel GetAllUsers()
        {
            try
            {
                return new OutputModel(1,"Success",userM);
            }
            catch (Exception ex)
            {
                throw new Exception("GetAllUsers " + ex.Message);
            }
        }

        public OutputModel GetUserByUsername(string username)
        {
            try
            {
                object userSel = null;

                if (username == null || username == "")
                {
                    return new OutputModel(-1, "Error", null);
                }

                foreach(var x in from x in
                                     userM where x.Username == username
                                 select new { x.Name,x.Surname,x.Username,x.Password,x.Age,x.IsActive })
                {
                    userSel = new { x };
                }

                return new OutputModel(1, "Success", userSel) ;
            }
            catch (Exception ex)
            {
                throw new Exception("GetUserByUsername " + ex.Message);
            }
        }

        public OutputModel InsUser(Users users) {
            try
            {
                if (users == null || 
                    users.Name == null || users.Surname == null || users.Username == null || users.Password == null 
                   )
                {
                    return new OutputModel(-1, "Error", null);
                }
                else
                {
                    foreach (var x in from x in userM where x.Username == users.Username select new {x.Username })
                    {
                        if (x.Username != null)
                        {
                            return new OutputModel(-1, "Error", null);
                        }
                    }

                    userM.Add(users);
                }

                

                return new OutputModel(1, "Success", null);
            }
            catch (Exception ex)
            {

                throw new Exception("InsUser " + ex.Message);
            }
        }

        
    }
}

