﻿using System;
using System.Text.Json;
using UnitTestTrainingConsoleApp.Entities;

namespace UnitTestTrainingConsoleApp
{
    class Program
    {
        

        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            // Type
            // String , int , decimal , boolean , date , double
            // string = "text"
            // int = 1
            // decimal = 1
            // boolean = true,false,
            // date = 20/08/65
            // double = 1.1


            string a = "text";
            int b = 0;
            decimal c = 1;
            bool d = true;
            DateTime dateTime = new DateTime();
            double e = 1.1;

            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(dateTime);
            Console.WriteLine(e);


            // if else , switch
            if(a == "texts")
            {
                Console.WriteLine("Correct!!!");
            }
            else
            {
                Console.WriteLine("InCorrect!!");
            }

            switch (b)
            {
                case 1:
                    {
                        Console.WriteLine("Test1");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Test2");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Any Case");
                        break;
                    }
            }


            // array = string[],int[],double[],...
            string[] cars = { "Lancer Evolution","Nissan Skyline R34","Toyota Supra" };
            int[] number = { 1, 2, 3, 4, 0, 0, 1 };
            double[] dou = { 11.1, 1.1, 2.1, 3.1, 4.1 };

            // for loop , foreach
            Console.WriteLine(cars[1]);
            for(int i = 0; i < cars.Length; i++)
            {
                Console.WriteLine(cars[i]);
            }


            Users users = new Users();
            users.Name = "Ratchaphoom";
            users.Surname = "Boonanaka";
            users.Username = "test1234";
            users.Password = "Password";
            users.Age = 27;
            users.IsActive = true;

            Console.WriteLine(users.Name);
            Console.WriteLine(users.Age);
            Console.WriteLine(users.IsActive);

            // json
            var obj = new {
                Code = 1,
                Message = "Success",
                Data = "Is Correct User"
            };

            var jsonString = JsonSerializer.Serialize(obj.ToString());
            Console.WriteLine(jsonString);

            Users users1 = new Users()
            {
                Name = "Thanos",
                Surname = "I dont' know",
                Age = 300,
                Username = "thanosza007",
                Password = "1234",
                IsActive = true
            };

            var objUser = new
            {
                Name = users1.Name,
                Surname = users1.Surname,
                Age = users1.Age,
                Username = users1.Username,
                Password = users1.Password,
                IsActive = users1.IsActive
            };

            string jsonUser = JsonSerializer.Serialize(objUser);
            Console.WriteLine(jsonUser);
            var dejsonUser = JsonSerializer.Deserialize<Users>(jsonUser);
            Console.WriteLine(dejsonUser.Name);



        }
    }
}

